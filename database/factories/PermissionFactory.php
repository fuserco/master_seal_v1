<?php

use Faker\Generator as Faker;
use Logistics\DB\Tenant\Permission;

$factory->define(Permission::class, function (Faker $faker) {
    return [
        'name' => 'Permission name',
        'slug' => 'permission-slug',
        'tenant_id' => 1,
        'header' => 'Permission',
    ];
});
