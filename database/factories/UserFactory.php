<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Logistics\DB\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstname,
        'last_name' => $faker->lastname,
        'email' => 'john.doe@acme.test',
        'password' => '$2y$10$hwIN3SaqJppzVSZ18Atit.7m3LgOsP8owJ/Pk5hJ/YMiBFJM4o4R6', //pwd123
        'remember_token' => str_random(10),
        'tenant_id' => null,
        'status' => 'A',
        'avatar' => null,
        'pid' => 'PID',
        'telephones' => '555-5555',
        'avatar' => 'avatar.png',
    ];
});

$factory->state(Logistics\DB\User::class, 'admin', function (Faker $faker) {
    return [
        'type' => 'A',
        'is_main_admin' => true,
        'permissions' => [],
        'position' => 1,
    ];
});

$factory->state(Logistics\DB\User::class, 'employee', function (Faker $faker) {
    return [
        'type' => 'E',
        'permissions' => [],
        'email' => $faker->unique()->safeEmail
    ];
});

$factory->state(Logistics\DB\User::class, 'clientuser', function (Faker $faker) {
    return [
        'type' => 'C',
        'permissions' => [],
        'email' => $faker->unique()->safeEmail,
        'client_id' => 1,
    ];
});
