<?php

use Faker\Generator as Faker;
use Logistics\DB\Tenant\Tenant;

$factory->define(Tenant::class, function (Faker $faker) {
    
    return [
        'domain' => 'acme.test',
        'name' => 'Acme Inc',
        'status' => 'A',
        'lang' => 'en',
        'address' => 'In the middle of nowhere',
        'telephones' => '555-5555',
        'emails' => "acme.test",
        'ruc' => "RUC",
        'dv' => "DV",
        'country_id' => 1,
        'logo' => 'logo.png',
        'timezone' => 'Continent/Country',
        'migration_mode' => false,
      

        'mail_driver' => env('MAIL_DRIVER', ''),
        'mail_host' => env('MAIL_HOST', ''),
        'mail_port' => env('MAIL_PORT', ''),
        'mail_username' => env('MAIL_USERNAME', ''),
        'mail_password' => env('MAIL_PASSWORD', ''),
        'mail_encryption' => env('MAIL_ENCRYPTION', ''),
        'mail_from_address' => env('MAIL_FROM_ADDRESS', ''),
        'mail_from_name' => env('MAIL_FROM_NAME', ''),
        'mailgun_domain' => env('MAILGUN_DOMAIN', ''),
        'mailgun_secret' => env('MAILGUN_SECRET', ''),

        'email_allowed_dup' => '',
    ];
});
