<?php

use Faker\Generator as Faker;
use Logistics\DB\Tenant\Country;

$factory->define(Country::class, function (Faker $faker) {
    return [
        'id' => 1,
        'name' => 'Country',
        'code' => 'XX',
        'iata' => 'YYY',
    ];
});
