<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Logistics\DB\Tenant\Country;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = Country::where('name', 'Country')->first();

        $departments = [
            ['name' => 'Department name', 'country_id' => $country->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'capital' => 'Department Capital', 'id' => 1],
        ];
        
        \DB::table('departments')->insert($departments);
    }
}
