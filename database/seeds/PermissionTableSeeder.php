<?php

use Illuminate\Database\Seeder;
use Logistics\DB\Tenant\Tenant;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = Tenant::whereId(1)->first();

        $tenant->permissions()->createMany([
            ["name" => "Permission name", "slug" => "permission-slug", "header" => "Permission",],
        ]);
    }
}
