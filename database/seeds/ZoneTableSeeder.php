<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Logistics\DB\Tenant\Country;

class ZoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = Country::where('name', 'Country')->first();

        $zones = [
            ['name' => 'Zone name', 'country_id' => $country->id, 'department_id' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        \DB::table('zones')->insert($zones);
    }
}
