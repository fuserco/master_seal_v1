<?php

use Logistics\DB\User;
use Illuminate\Database\Seeder;
use Logistics\DB\Tenant\Tenant;
use Logistics\DB\Tenant\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = Tenant::whereId(1)->first();
        $branch = $tenant->branches->where('name', '=', 'Branch name')->first();

        $admin = factory(User::class)->states('admin')->create([
            'tenant_id' => $tenant->id,
            'email' => 'admin@acme.test',
            'permissions' => Permission::all()->pluck('slug')->toArray(),
        ]);
        $admin->branches()->sync([$branch->id]);
        $admin->branchesForInvoice()->sync([$branch->id]);
    }
}
