<?php

use Illuminate\Database\Seeder;
use Logistics\DB\Tenant\Tenant;
use Logistics\DB\Tenant\Branch;

class TenantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = factory(Tenant::class)->create();

        $tenant->remoteAddresses()->createMany([
            ['type' => 'A', 'address' => 'address a', 'telephones' => '555-5555', 'status' => 'A', ],
            ['type' => 'M', 'address' => 'address m', 'telephones' => '555-5556', 'status' => 'A', ],
        ]);

        $tenant->conditions()->createMany([
            ['type' => 'W', 'content' => 'The conditions for warehouse receipt', 'status' => 'A', ],
            ['type' => 'I', 'content' => 'The conditions for invoices', 'status' => 'A', ],
        ]);

        $tenant->branches()->createMany([
            [
                'name' => 'Branch name',
                'address' => '123 street ave',
                'telephones' => '555-5482',
                'emails' => "contact@acme.test",
                'code' => 'XXX',
                'initial' => 'XXX',
                'real_price' => 2.50,
                'vol_price' => 1.75,
                'dhl_price' => 2.25,
                'maritime_price' => 250,
                'should_invoice' => true,
            ]
        ]);
    }
}
